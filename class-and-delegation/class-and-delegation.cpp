// class-and-delegation.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include "Car.h"

int main()
{
	Car cc{ "Volvo S60", 2, 170 };

	SportsCar sc{ "Jaguar", 4, 210 };

	Car cval = sc;
	// This does not because of object slicing.  The C++ compiler cuts the 
	// type information of any descendants of Car, the SportsCar becomes a Car
	// and loses all type information of SportsCar

	Car& cobj = sc;
	cobj.move();
	Car *cptr = &sc;
	cptr->move();
	// In these two examples, the reference and pointer types link to the Car type
	// but the object has not been sliced, it remains as a SportsCar
	// C++ type system is by default non polymorphic, so when T::move() called it
	// always results in the T::move() of of the current reference and pointer type

	// Adding virtual to a method on a base type (does nto have to be the top of the tree
	// will result in that becoming polymorphic.  The base type now has a virtaul table
}
