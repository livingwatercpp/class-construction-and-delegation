#pragma once

#include <string>

using namespace std;

class Engine
{
public:
	Engine(int capacity, int power ) :itsCapacity(capacity), itsPower( power) 
	{
		std::cout << "Engine::Engine(int capacity, int power)" << std::endl;
	}
	Engine()
	{
		std::cout << "Engine::Engine()" << std::endl;
	}

private:
	int itsCapacity;
	int itsPower;
};

class Car
{
public:
	Car(string regNo, int size, int power) :itsRegNo(regNo), itsEngine( size, power )
	{
		std::cout << "Car::Car(string regNo)" << std::endl;
	}

	virtual	void	move()
	{
		std::cout << "Car::move()" << std::endl;
	}

private:
	string itsRegNo;
	Engine itsEngine;
};

class SportsCar : public Car
{
public:
	SportsCar(string regNo, int size, int power):Car( regNo, size, power)
	{}

	void	move()	override // a marker to indicate that a parent method is virtual
	{
		std::cout << "SportsCar::move()" << std::endl;
	}
};